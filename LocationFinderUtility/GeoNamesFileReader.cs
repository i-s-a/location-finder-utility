﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using LocationFinderUtility.Models;
using LocationFinderUtility.Repositories;

namespace LocationFinderUtility
{
    class GeoNamesFileReader : IFileReader
    {
        private readonly ILocationsRepository<long> _repository = null;

        public GeoNamesFileReader(ILocationsRepository<long> repository)
        {
            _repository = repository;
        }

        public async Task PopulateCountryCodes(string countryCodeFilename)
        {
            using (StreamReader streamReader = new StreamReader(countryCodeFilename, Encoding.UTF8))
            {
                string line = await streamReader.ReadLineAsync();   //Ignore first line which is just the headers
                line = await streamReader.ReadLineAsync();
                while (line != null)
                {
                    if (line.Length > 0)
                    {
                        string[] components = line.Split('\t');

                        string name = components[0];
                        name = name.Trim();

                        string alpha2Code = components[1];
                        alpha2Code = alpha2Code.Trim();

                        string alpha3Code = components[2];
                        alpha3Code = alpha3Code.Trim();

                        string numeric = components[3];
                        numeric = numeric.Trim();
                        int numericCode = Convert.ToInt32(numeric);

                        Console.WriteLine($"{name}");

                        CountryCode countryCode = await _repository.GetCountryCode(alpha2Code);

                        if (countryCode == null)
                        {
                            countryCode = new CountryCode
                            {
                                Id = alpha2Code,
                                Name = name,
                                Alpha3Code = alpha3Code,
                                NumericCode = numericCode
                            };

                            _repository.AddCountryCode(countryCode);
                        }
                        else
                        {
                            countryCode.Name = name;
                            countryCode.Alpha3Code = alpha3Code;
                            countryCode.NumericCode = numericCode;

                            _repository.UpdateCountryCode(countryCode);
                        }
                    }

                    line = await streamReader.ReadLineAsync();
                }

            }
        }

        public async Task PopulateFeatureCodes(string featureCodeFilename)
        {
            using (StreamReader streamReader = new StreamReader(featureCodeFilename, Encoding.UTF8))
            {
                string line = await streamReader.ReadLineAsync();
                while (line != null)
                {
                    if (line.Length > 0)
                    {
                        string[] components = line.Split('\t');

                        string[] featureComponents = components[0].Split('.');
                        //string class = featureComponents[0];   //Ignore feature class for now
                        string code = featureComponents[1];   //We use the feature code for the ID
                        string name = components[1];

                        Console.WriteLine($"{code}");

                        FeatureCode featureCode = await _repository.GetFeatureCode(code);

                        if (featureCode == null)
                        {
                            featureCode = new FeatureCode
                            {
                                Id = code,
                                Name = name.ToLower()
                            };

                            _repository.AddFeatureCode(featureCode);
                        }
                        else
                        {
                            featureCode.Name = name.ToLower();

                            _repository.UpdateFeatureCode(featureCode);
                        }

                    }

                    line = await streamReader.ReadLineAsync();
                }
            }
        }

        public async Task PopulatePlaceNames(string placesFilename)
        {
            StringBuilder errors = new StringBuilder();

            using (StreamReader streamReader = new StreamReader(placesFilename, Encoding.UTF8))
            {
                string line = await streamReader.ReadLineAsync();
                while (line != null)
                {
                    try
                    {
                        if (line.Length > 0)
                        {
                            string[] components = line.Split('\t');

                            //See: http://download.geonames.org/export/dump/readme.txt
                            long id = Convert.ToInt64(components[0]);
                            string name = components[1];
                            double latitude = Convert.ToDouble(components[4]);
                            double longitude = Convert.ToDouble(components[5]);
                            string featureClass = components[6];
                            string featureCode = components[7];
                            string countryCode = components[8];

                            CountryCode curCountryCode = await _repository.GetCountryCode(countryCode);
                            if (curCountryCode == null)
                            {
                                if (string.IsNullOrEmpty(countryCode))
                                    countryCode = "[EMPTY]";

                                throw new Exception($"Failed to find country code {countryCode}");
                            }

                            FeatureCode curFeatureCode = await _repository.GetFeatureCode(featureCode);
                            if (curFeatureCode == null)
                            {
                                if (string.IsNullOrEmpty(featureCode))
                                    featureCode = "[EMPTY]";

                                throw new Exception($"Failed to find feature code {featureCode}");
                            }

                            Console.WriteLine($"{name}");

                            Location location = await _repository.GetLocation(id);

                            if (location == null)
                            {
                                location = new Location
                                {
                                    Id = id,
                                    Name = name,
                                    CountryCode = curCountryCode,
                                    FeatureCode = curFeatureCode,
                                    Latitude = latitude,
                                    Longitude = longitude
                                };

                                _repository.AddLocation(location);
                            }
                            else
                            {

                                location.Id = id;
                                location.Name = name;
                                location.CountryCode = curCountryCode;
                                location.FeatureCode = curFeatureCode;
                                location.Latitude = latitude;
                                location.Longitude = longitude;

                                _repository.UpdateLocation(location);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        errors.AppendLine(ex.Message);
                    }

                    line = await streamReader.ReadLineAsync();
                }
            }

            if (errors.Length > 0)
            {
                System.Console.WriteLine("ERRORS");
                System.Console.WriteLine(errors.ToString());
            }
        }

        public Task Save()
        {
            return _repository.Save();
        }
    }
}
