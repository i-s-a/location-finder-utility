﻿using System;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;
using LocationFinderUtility.Repositories;

namespace LocationFinderUtility
{
    class Program
    {
        private static ILocationsRepository<long> CreateLocationsRepository(string connectionString)
        {
            var options = new DbContextOptionsBuilder<LocationsContext>()
                .UseSqlite($"Data Source={connectionString}")
                .Options;

            var locationsContext = new LocationsContext(options);
            locationsContext.Database.EnsureCreated();

            return new SqliteLocationsRepository(locationsContext);
        }

        private static IFileReader CreateFileReader(ILocationsRepository<long> repository)
        {
            return new GeoNamesFileReader(repository);
        }

        private static async Task ProcessCommandLineParams(string connectionString, string placesFilename,
            string featureCodeFilename, string countryCodeFilename)
        {
            ILocationsRepository<long> repository = CreateLocationsRepository(connectionString);
            IFileReader fileReader = CreateFileReader(repository);
   
            if (!string.IsNullOrEmpty(countryCodeFilename) || !string.IsNullOrEmpty(featureCodeFilename))
            {
                if (!string.IsNullOrEmpty(countryCodeFilename))
                {
                    await fileReader.PopulateCountryCodes(countryCodeFilename);
                }

                if (!string.IsNullOrEmpty(featureCodeFilename))
                {
                    await fileReader.PopulateFeatureCodes(featureCodeFilename);
                }

                await fileReader.Save();
            }

            if (!string.IsNullOrEmpty(placesFilename))
            {
                await fileReader.PopulatePlaceNames(placesFilename);
                await fileReader.Save();
            }
        }

        static async Task Main(string[] args)
        {
            try
            {
                //await ProcessCommandLineParams(
                //    @"..\..\..\..\LocationFinderClientTest\TestFiles\locations.db",
                //    @"..\..\..\..\LocationFinderClientTest\TestFiles\GB.txt",
                //    @"..\..\..\..\LocationFinderClientTest\TestFiles\featureCodes_en.txt",
                //    @"..\..\..\..\LocationFinderClientTest\TestFiles\CountryCodes.csv");

                string connectionString = null;     //The database filename
                string countryCodeFilename = null;
                string featureCodeFilename = null;
                string placesFilename = null;

                if (args.Length % 2 == 0)
                {
                    int counter = 0;
                    while (counter < args.Length)
                    {
                        string flag = args[counter];

                        if (flag == "-cc")
                        {
                            ++counter;
                            countryCodeFilename = args[counter];
                        }
                        else if (flag == "-fc")
                        {
                            ++counter;
                            featureCodeFilename = args[counter];
                        }
                        else if (flag == "-i")
                        {
                            ++counter;
                            placesFilename = args[counter];
                        }
                        else if (flag == "-c")
                        {
                            ++counter;
                            connectionString = args[counter];
                        }
                        else
                        {
                            Console.WriteLine("Unrecognised command line argument");
                            Environment.Exit(-1);
                        }

                        ++counter;
                    }

                    await ProcessCommandLineParams(connectionString, placesFilename, featureCodeFilename,
                        countryCodeFilename);
                }
                else
                {
                    Console.WriteLine("Invalid command line parameters");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
