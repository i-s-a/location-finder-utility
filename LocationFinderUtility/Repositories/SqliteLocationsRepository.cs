﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using LocationFinderUtility.Models;

namespace LocationFinderUtility.Repositories
{
    class SqliteLocationsRepository : ILocationsRepository<long>
    {
        private readonly LocationsContext _locationsContext = null;

        public SqliteLocationsRepository(LocationsContext context)
        {
            _locationsContext = context;
        }

        public void AddLocation(Location location)
        {
            _locationsContext.Locations.Add(location);
        }

        public void AddCountryCode(CountryCode countryCode)
        {
            _locationsContext.CountryCodes.Add(countryCode);
        }

        public void AddFeatureCode(FeatureCode featureCode)
        {
            _locationsContext.FeatureCodes.Add(featureCode);
        }

        public void DeleteLocation(Location location)
        {
            _locationsContext.Locations.Remove(location);
        }

        public void DeleteCountryCode(CountryCode countryCode)
        {
            _locationsContext.CountryCodes.Remove(countryCode);
        }

        public void DeleteFeatureCode(FeatureCode featureCode)
        {
            _locationsContext.FeatureCodes.Remove(featureCode);
        }

        public Task<Location> GetLocation(long id)
        {
            return _locationsContext
                .Locations
                .Include(c => c.CountryCode)
                .Include(f => f.FeatureCode)
                .SingleOrDefaultAsync(x => x.Id == id);
        }

        public Task<CountryCode> GetCountryCode(string id)
        {
            return _locationsContext
                .CountryCodes
                .SingleOrDefaultAsync(x => x.Id.ToLower() == id.ToLower());
        }

        public Task<FeatureCode> GetFeatureCode(string id)
        {
            return _locationsContext
                .FeatureCodes
                .SingleOrDefaultAsync(x => x.Id.ToLower() == id.ToLower());
        }

        public async Task Save()
        {
            await _locationsContext.SaveChangesAsync();
        }

        public void UpdateLocation(Location location)
        {
            _locationsContext.Locations.Update(location);
        }

        public void UpdateCountryCode(CountryCode countryCode)
        {
            _locationsContext.CountryCodes.Update(countryCode);
        }

        public void UpdateFeatureCode(FeatureCode featureCode)
        {
            _locationsContext.FeatureCodes.Update(featureCode);
        }
    }
}
