﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using LocationFinderUtility.Models;

namespace LocationFinderUtility.Repositories
{
    interface ILocationsRepository<T>
    {
        void AddLocation(Location location);
        void DeleteLocation(Location location);
        void UpdateLocation(Location location);
        Task<Location> GetLocation(T id);

        void AddCountryCode(CountryCode countryCode);
        void DeleteCountryCode(CountryCode countryCode);
        void UpdateCountryCode(CountryCode countryCode);
        Task<CountryCode> GetCountryCode(string id);

        void AddFeatureCode(FeatureCode featureCode);
        void DeleteFeatureCode(FeatureCode featureCode);
        void UpdateFeatureCode(FeatureCode featureCode);
        Task<FeatureCode> GetFeatureCode(string id);

        Task Save();
    }
}
