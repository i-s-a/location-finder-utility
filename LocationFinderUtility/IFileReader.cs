﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace LocationFinderUtility
{
    interface IFileReader
    {
        Task PopulateCountryCodes(string countryCodeFilename);
        Task PopulateFeatureCodes(string featureCodeFilename);
        Task PopulatePlaceNames(string placesFilename);
        Task Save();
    }
}
