﻿using Microsoft.EntityFrameworkCore;
using LocationFinderUtility.Models;

namespace LocationFinderUtility
{
    public class LocationsContext : DbContext
    {
        public LocationsContext(DbContextOptions<LocationsContext> options)
            : base(options)
        {
        }

        public DbSet<Location> Locations { get; set; }
        public DbSet<CountryCode> CountryCodes { get; set; }
        public DbSet<FeatureCode> FeatureCodes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Location>().HasIndex(loc => loc.Name);

            //Add indexes to the latitude and longitude as we allow location searches within a
            //user-supplied radius which filter on the latitude and longitude
            modelBuilder.Entity<Location>()
                .HasIndex(loc => loc.Latitude);

            modelBuilder.Entity<Location>()
                .HasIndex(loc => loc.Longitude);

            //Create an index on the name as well as we will allow searches on country name as well as
            //on ISO code
            modelBuilder.Entity<CountryCode>()
                .HasIndex(code => code.Name)
                .IsUnique();
        }
    }
}
