﻿using System.ComponentModel.DataAnnotations;

namespace LocationFinderUtility.Models
{
    public class Location
    {
        [Key]
        public long Id { get; set; } = 0;
        public string Name { get; set; }
        //public string CountryCodeId { get; set; }

        //Making the country code and feature code Required will ensure that both values exist and EF
        //will generate an INNER JOIN when querying the tables instead of a LEFT JOIN
        [Required]
        public CountryCode CountryCode { get; set; }
        [Required]
        public FeatureCode FeatureCode { get; set; }

        public double Latitude { get; set; }
        public double Longitude { get; set; }
    }
}
