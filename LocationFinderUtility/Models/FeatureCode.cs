﻿using System.ComponentModel.DataAnnotations;

namespace LocationFinderUtility.Models
{
    public class FeatureCode
    {
        [Key]
        public string Id { get; set; }
        public string Name { get; set; }
    }
}
