﻿using System;
using System.Threading.Tasks;
using System.IO;
using System.Text;
using LocationFinderClient;
using LocationFinderClient.Requests;
using GeoMathLib;

namespace LocationFinderClientTest
{
    class Program
    {

        private const string BaseUrl = "http://localhost:5000/api/v1/";

        static private async Task PopulateFile(string filename, string serverAddress, string version)
        {
            int counter = 0;

            string baseUri = $"{serverAddress}/api/v{version}/";
            using (var connection = new LocationFinderClientConnection(baseUri))
            {
                try
                {
                    using (StreamReader streamReader = new StreamReader(filename, Encoding.UTF8))
                    {
                        string line = await streamReader.ReadLineAsync();
                        while (line != null)
                        {
                            string[] components = line.Split('\t');

                            //See: http://download.geonames.org/export/dump/readme.txt
                            long id = Convert.ToInt64(components[0]);
                            string name = components[1];
                            double latitude = Convert.ToDouble(components[4]);
                            double longitude = Convert.ToDouble(components[5]);
                            string featureClass = components[6];
                            string featureCode = components[7];
                            string countryCode = components[8];

                            try
                            {
                                var response = await connection.CreateOrUpdate(id, new UpdateLocationRequest
                                {
                                    Name = name,
                                    FeatureCodeId = featureCode,
                                    CountryCodeId = countryCode,
                                    Coordinates = new GeoCoordinates { Latitude = latitude, Longitude = longitude }
                                });

                                System.Console.WriteLine($"{++counter}, {response.Id}, {response.Name}, {response.HRef}");
                            }
                            catch (Exception ex)
                            {
                                System.Console.WriteLine($"{++counter}, {name}");
                                System.Console.WriteLine(ex.Message);
                            }

                            line = await streamReader.ReadLineAsync();
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine("Error: ");
                    System.Console.WriteLine(ex.Message);
                }
            }
        }

        static private async Task PopulateTestFile(string filename)
        {
            int counter = 0;

            using (var connection = new LocationFinderClientConnection(BaseUrl))
            {
                try
                {
                    using (StreamReader streamReader = new StreamReader(filename, Encoding.UTF8))
                    {
                        string line = await streamReader.ReadLineAsync();
                        while (line != null)
                        {
                            string[] components = line.Split('\t');

                            //See: http://download.geonames.org/export/dump/readme.txt
                            long id = Convert.ToInt64(components[0]);
                            string name = components[1];
                            double latitude = Convert.ToDouble(components[4]);
                            double longitude = Convert.ToDouble(components[5]);
                            string featureClass = components[6];
                            string featureCode = components[7];
                            string countryCode = components[8];

                            try
                            {
                                var response = await connection.CreateOrUpdate(id, new UpdateLocationRequest
                                {
                                    Name = name,
                                    CountryCodeId = countryCode,
                                    FeatureCodeId = featureCode,
                                    Coordinates = new GeoCoordinates { Latitude = latitude, Longitude = longitude }
                                });

                                System.Console.WriteLine($"{++counter}, {response.Id}, {response.Name}, {response.HRef}");
                            }
                            catch (Exception ex)
                            {
                                System.Console.WriteLine($"{++counter}, {name}");
                                System.Console.WriteLine(ex.Message);
                            }

                            line = await streamReader.ReadLineAsync();
                        }
                    }
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                }
            }
        }

        static async Task GetLocations(int offset, int limit)
        {
            System.Console.WriteLine("GetLocations");

            using (var connection = new LocationFinderClientConnection(BaseUrl))
            {
                try
                {
                    var response = await connection.GetLocations(offset, limit);
                    System.Console.WriteLine($"{response.Count}");

                    foreach (var r in response)
                    {
                        System.Console.WriteLine(
                            $"ID: {r.Id},  " +
                            $"Name: {r.Name},  " +
                            $"Latitude: {r.Coordinates.Latitude}, " +
                            $"Longitude: {r.Coordinates.Longitude}\n" +
                            $"Country Code: {r.CountryCodeId} " +
                            $"Feature Code: {r.FeatureCodeId}");
                    }
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                }
            }
        }

        static async Task GetLocations(int offset, int limit, SortOrder sortOrder, bool descending)
        {
            System.Console.WriteLine("GetLocations");

            using (var connection = new LocationFinderClientConnection(BaseUrl))
            {
                try
                {
                    var response = await connection.GetLocations(offset, limit, sortOrder, descending);
                    System.Console.WriteLine($"{response.Count}");

                    foreach (var r in response)
                    {
                        System.Console.WriteLine(
                            $"ID: {r.Id},  " +
                            $"Name: {r.Name},  " +
                            $"Latitude: {r.Coordinates.Latitude}, " +
                            $"Longitude: {r.Coordinates.Longitude}\n" +
                            $"Country Code: {r.CountryCodeId} " +
                            $"Feature Code: {r.FeatureCodeId}");
                    }
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                }
            }
        }

        static async Task GetLocationsCount()
        {
            System.Console.WriteLine("GetLocationsCount");

            using (var connection = new LocationFinderClientConnection(BaseUrl))
            {
               try
                {
                    var response = await connection.GetLocationsCount();
                    System.Console.WriteLine($"{response}");
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                }
            }
        }

        static async Task GetById(long id)
        {
            System.Console.WriteLine("GetById");

            using (var connection = new LocationFinderClientConnection(BaseUrl))
            {
                try
                {
                    var response = await connection.GetLocationById(id);

                    System.Console.WriteLine(
                        $"ID: {response.Id},  " +
                        $"Name: {response.Name},  " +
                        $"Latitude: {response.Coordinates.Latitude}, " +
                        $"Longitude: {response.Coordinates.Longitude}\n" +
                        $"Country Code: {response.CountryCodeId} " +
                        $"Feature Code: {response.FeatureCodeId}");
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                }
            }
        }

        static async Task GetByName(string name, string countryCodeId)
        {
            System.Console.WriteLine("GetByName");

            using (var connection = new LocationFinderClientConnection(BaseUrl))
            {
                try
                {
                    var response = await connection.GetLocationByName(name, countryCodeId, null, null);
                    System.Console.WriteLine($"{response.Count}");

                    foreach (var r in response)
                    {
                        System.Console.WriteLine(
                            $"ID: {r.Id},  " +
                            $"Name: {r.Name},  " +
                            $"Latitude: {r.Coordinates.Latitude}, " +
                            $"Longitude: {r.Coordinates.Longitude}\n" +
                            $"Country Code: {r.CountryCodeId} " +
                            $"Feature Code: {r.FeatureCodeId}");
                    }
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                }
            }
        }

        static async Task GetByName(string name, string countryCodeId, SortOrder sortOrder, bool descending)
        {
            System.Console.WriteLine("GetByName");

            using (var connection = new LocationFinderClientConnection(BaseUrl))
            {
                try
                {
                    var response = await connection.GetLocationByName(name, countryCodeId, null, null, sortOrder, descending);
                    System.Console.WriteLine($"{response.Count}");

                    foreach (var r in response)
                    {
                        System.Console.WriteLine(
                            $"ID: {r.Id},  " +
                            $"Name: {r.Name},  " +
                            $"Latitude: {r.Coordinates.Latitude}, " +
                            $"Longitude: {r.Coordinates.Longitude}\n" +
                            $"Country Code: {r.CountryCodeId} " +
                            $"Feature Code: {r.FeatureCodeId}");
                    }
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                }
            }
        }

        static async Task GetByNameCount(string name, string countryCodeId)
        {
            System.Console.WriteLine("GetByNameCount");

            using (var connection = new LocationFinderClientConnection(BaseUrl))
            {
                try
                {
                    var response = await connection.GetLocationByNameCount(name, countryCodeId);
                    System.Console.WriteLine($"{response}");
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                }
            }
        }

        static async Task GetBySimilarName(string name, string countryCodeId, int? offset, int? limit)
        {
            System.Console.WriteLine("GetBySimilarName");

            using (var connection = new LocationFinderClientConnection(BaseUrl))
            {
                try
                {
                    var response = await connection.GetLocationsBySimilarName(name, countryCodeId, offset, limit);
                    System.Console.WriteLine($"{response.Count}");

                    foreach (var r in response)
                    {
                        System.Console.WriteLine(
                            $"ID: {r.Id},  " +
                            $"Name: {r.Name},  " +
                            $"Latitude: {r.Coordinates.Latitude}, " +
                            $"Longitude: {r.Coordinates.Longitude}\n" +
                            $"Country Code: {r.CountryCodeId} " +
                            $"Feature Code: {r.FeatureCodeId}");
                    }
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                }
            }
        }

        static async Task GetBySimilarName(string name, string countryCodeId, int? offset, int? limit, SortOrder sortOrder, bool descending)
        {
            System.Console.WriteLine("GetBySimilarName");

            using (var connection = new LocationFinderClientConnection(BaseUrl))
            {
                try
                {
                    var response = await connection.GetLocationsBySimilarName(name, countryCodeId, offset, limit, sortOrder, descending);
                    System.Console.WriteLine($"{response.Count}");

                    foreach (var r in response)
                    {
                        System.Console.WriteLine(
                            $"ID: {r.Id},  " +
                            $"Name: {r.Name},  " +
                            $"Latitude: {r.Coordinates.Latitude}, " +
                            $"Longitude: {r.Coordinates.Longitude}\n" +
                            $"Country Code: {r.CountryCodeId} " +
                            $"Feature Code: {r.FeatureCodeId}");
                    }
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                }
            }
        }

        static async Task GetBySimilarNameCount(string name, string countryCodeId)
        {
            System.Console.WriteLine("GetBySimilarNameCount");

            using (var connection = new LocationFinderClientConnection(BaseUrl))
            {
                try
                {
                    var response = await connection.GetLocationsBySimilarNameCount(name, countryCodeId);
                    System.Console.WriteLine($"{response}");
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                }
            }
        }

        static async Task GetLocationsWithinBoundary(GeoCoordinates location, double distanceKm, string featureCodeId)
        {
            System.Console.WriteLine("GetLocationsWithinBoundary");

            using (var connection = new LocationFinderClientConnection(BaseUrl))
            {
                try
                {
                    var response = await connection.GetLocationsWithinBoundary(location, distanceKm, featureCodeId, 
                        null, null);
                    System.Console.WriteLine($"{response.Count}");

                    foreach (var r in response)
                    {
                        System.Console.WriteLine(
                            $"ID: {r.Id},  " +
                            $"Name: {r.Name},  " +
                            $"Distance Km: {r.DistanceKm}, " +
                            $"Miles: {GeoMath.KmToMiles(r.DistanceKm)}\n" +
                            $"Country Code: {r.CountryCodeId} " +
                            $"Feature Code: {r.FeatureCodeId}");
                    }
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                }
            }
        }

        static async Task GetLocationsWithinBoundary(GeoCoordinates location, double distanceKm, string featureCodeId,
            SortOrder sortOrder, bool descending)
        {
            System.Console.WriteLine("GetLocationsWithinBoundary");

            using (var connection = new LocationFinderClientConnection(BaseUrl))
            {
                try
                {
                    var response =
                        await connection.GetLocationsWithinBoundary(location, distanceKm, featureCodeId, null, null, sortOrder, descending);

                    System.Console.WriteLine($"{response.Count}");

                    foreach (var r in response)
                    {
                        System.Console.WriteLine(
                            $"ID: {r.Id},  " +
                            $"Name: {r.Name},  " +
                            $"Distance Km: {r.DistanceKm}, " +
                            $"Miles: {GeoMath.KmToMiles(r.DistanceKm)}\n" +
                            $"Country Code: {r.CountryCodeId} " +
                            $"Feature Code: {r.FeatureCodeId}");
                    }
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                }
            }
        }

        static async Task GetLocationsWithinBoundaryCount(GeoCoordinates location, double distanceKm, string featureCodeId)
        {
            System.Console.WriteLine("GetLocationsWithinBoundaryCount");

            using (var connection = new LocationFinderClientConnection(BaseUrl))
            {
                try
                {
                    var response = await connection.GetLocationsWithinBoundaryCount(location, distanceKm, featureCodeId);
                    System.Console.WriteLine($"{response}");
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                }
            }
        }

        static async Task GetByFeatureCodeId(string feature_id, string country_id)
        {
            System.Console.WriteLine("GetByFeatureCodeId");

            using (var connection = new LocationFinderClientConnection(BaseUrl))
            {
                try
                {
                    var response = await connection.GetByFeatureCodeId(feature_id, country_id, null, null, SortOrder.NAME, false);
                    System.Console.WriteLine($"{response.Count}");

                    foreach (var r in response)
                    {
                        System.Console.WriteLine(
                            $"ID: {r.Id},  " +
                            $"Name: {r.Name},  " +
                            $"Latitude: {r.Coordinates.Latitude}, " +
                            $"Longitude: {r.Coordinates.Longitude}\n" +
                            $"Country Code: {r.CountryCodeId} " +
                            $"Feature Code: {r.FeatureCodeId}");
                    }
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                }
            }
        }

        static async Task GetByCountryCodeId(string country_id)
        {
            System.Console.WriteLine("GetByFeatureCodeId");

            using (var connection = new LocationFinderClientConnection(BaseUrl))
            {
                try
                {
                    var response = await connection.GetByCountryCodeId(country_id, null, null, SortOrder.NAME, false);
                    System.Console.WriteLine($"{response.Count}");

                    foreach (var r in response)
                    {
                        System.Console.WriteLine(
                            $"ID: {r.Id},  " +
                            $"Name: {r.Name},  " +
                            $"Latitude: {r.Coordinates.Latitude}, " +
                            $"Longitude: {r.Coordinates.Longitude}\n" +
                            $"Country Code: {r.CountryCodeId} " +
                            $"Feature Code: {r.FeatureCodeId}");
                    }
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                }
            }
        }

        //----------------------------------------------------------------------
        static async Task GetCountryCode(string id)
        {
            System.Console.WriteLine("GetCountryCode");

            using (var connection = new LocationFinderClientConnection(BaseUrl))
            {
                try
                {
                    var response = await connection.GetCountryCode(id);
                    System.Console.WriteLine($"{response.Id}, " +
                        $"{response.Name}, " +
                        $"{response.Alpha3Code}, " +
                        $"{response.NumericCode}");
                }
                catch(Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                }
            }
        }

        static async Task GetCountryCodes()
        {
            System.Console.WriteLine("GetCountryCodes");

            using (var connection = new LocationFinderClientConnection(BaseUrl))
            {
                try
                {
                    var response = await connection.GetCountryCodes();
                    foreach(var countryCode in response)
                    {
                        System.Console.WriteLine($"{countryCode.Id}, " +
                            $"{countryCode.Name}, " +
                            $"{countryCode.Alpha3Code}, " +
                            $"{countryCode.NumericCode}");
                    }

                    System.Console.WriteLine($"{response.Count}");
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                }
            }
        }

        static async Task GetFeatureCode(string id)
        {
            System.Console.WriteLine("GetFeatureCode");

            using (var connection = new LocationFinderClientConnection(BaseUrl))
            {
                try
                {
                    var response = await connection.GetFeatureCode(id);
                    System.Console.WriteLine($"{response.Id}, " +
                        $"{response.Name}");
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                }
            }
        }

        static async Task GetFeatureCodes()
        {
            System.Console.WriteLine("GetFeatureCodes");

            using (var connection = new LocationFinderClientConnection(BaseUrl))
            {
                try
                {
                    var response = await connection.GetFeatureCodes();
                    foreach (var countryCode in response)
                    {
                        System.Console.WriteLine($"{countryCode.Id}, " +
                            $"{countryCode.Name}");
                    }

                    System.Console.WriteLine($"{response.Count}");
                }
                catch (Exception ex)
                {
                    System.Console.WriteLine(ex.Message);
                }
            }
        }

        static async Task Main(string[] args)
        {

            //string testFilename = @"..\..\..\TestFiles\TestData.txt";
            //await PopulateTestFile(testFilename);

            await GetLocationsWithinBoundary(new GeoCoordinates
            {
                Latitude = 50.86155,
                Longitude = -0.08361
            }, 5, null);
            System.Console.WriteLine();

            //await GetLocationsWithinBoundary(new GeoCoordinates
            //{
            //    Latitude = 50.86155,
            //    Longitude = -0.08361
            //}, 1, null);
            //System.Console.WriteLine();

            //await GetLocationsWithinBoundary(new GeoCoordinates
            //{
            //    Latitude = 50.86155,
            //    Longitude = -0.08361
            //}, 5, null, SortOrder.LATITUDE, false);
            //System.Console.WriteLine();

            //await GetLocationsWithinBoundary(new GeoCoordinates
            //{
            //    Latitude = 50.86155,
            //    Longitude = -0.08361
            //}, 1, null, SortOrder.LATITUDE, false);
            //System.Console.WriteLine();

            //await GetLocationsWithinBoundary(new GeoCoordinates
            //{
            //    Latitude = 50.86155,
            //    Longitude = -0.08361
            //}, 5, null, SortOrder.LONGITUDE, true);
            //System.Console.WriteLine();

            //await GetLocationsWithinBoundary(new GeoCoordinates
            //{
            //    Latitude = 50.86155,
            //    Longitude = -0.08361
            //}, 1, null, SortOrder.LONGITUDE, true);
            //System.Console.WriteLine();

            //await GetLocationsWithinBoundary(new GeoCoordinates
            //{
            //    Latitude = 50.86155,
            //    Longitude = -0.08361
            //}, 2, "ppl");
            //System.Console.WriteLine();

            ////-------------------------------------------------------------

            //await GetLocationsWithinBoundaryCount(new GeoCoordinates
            //{
            //    Latitude = 50.86155,
            //    Longitude = -0.08361
            //}, 5, null);
            //System.Console.WriteLine();

            //await GetLocationsWithinBoundaryCount(new GeoCoordinates
            //{
            //    Latitude = 50.86155,
            //    Longitude = -0.08361
            //}, 1, null);
            //System.Console.WriteLine();

            //await GetLocationsWithinBoundaryCount(new GeoCoordinates
            //{
            //    Latitude = 50.86155,
            //    Longitude = -0.08361
            //}, 2, "ppl");
            //System.Console.WriteLine();

            ////-------------------------------------------------------------

            //await GetLocations(0, 5);
            //System.Console.WriteLine();

            //await GetLocations(5, 5);
            //System.Console.WriteLine();

            //await GetLocations(0, 10);
            //System.Console.WriteLine();

            //await GetLocations(0, 5, SortOrder.ID, false);
            //System.Console.WriteLine();

            //await GetLocations(5, 5, SortOrder.ID, false);
            //System.Console.WriteLine();

            //await GetLocations(0, 10, SortOrder.ID, false);
            //System.Console.WriteLine();

            //await GetLocations(0, 5, SortOrder.ID, true);
            //System.Console.WriteLine();

            //await GetLocations(5, 5, SortOrder.ID, true);
            //System.Console.WriteLine();

            //await GetLocations(0, 10, SortOrder.ID, true);
            //System.Console.WriteLine();

            ////-------------------------------------------------------------

            //await GetLocationsCount();
            //System.Console.WriteLine();

            ////-------------------------------------------------------------

            //await GetById(2651208);
            //System.Console.WriteLine();

            ////-------------------------------------------------------------

            //await GetByName("London Victoria Station", "GB");
            //System.Console.WriteLine();

            //await GetByName("London Victoria Station", "GB", SortOrder.ID, true);
            //System.Console.WriteLine();

            ////-------------------------------------------------------------

            //await GetByNameCount("London Victoria Station", "GB");
            //System.Console.WriteLine();

            ////-------------------------------------------------------------

            //await GetBySimilarName("Victoria Station", "GB", null, null);
            //System.Console.WriteLine();

            ////-------------------------------------------------------------

            //await GetBySimilarNameCount("Victoria Station", "GB");
            //System.Console.WriteLine();

            ////-------------------------------------------------------------

            //await GetBySimilarName("Victoria Station", "EG", null, null);
            //System.Console.WriteLine();

            //await GetBySimilarNameCount("Victoria Station", "EG");
            //System.Console.WriteLine();

            ////-------------------------------------------------------------

            //await GetBySimilarName("a", "GB", 15, 5);
            //System.Console.WriteLine();
            //await GetBySimilarName("a", "GB", 20, 5);
            //System.Console.WriteLine();
            //await GetBySimilarName("a", "GB", 15, 10);
            //System.Console.WriteLine();

            //await GetBySimilarName("a", "GB", 15, 5, SortOrder.NAME, true);
            //System.Console.WriteLine();
            //await GetBySimilarName("a", "GB", 20, 5, SortOrder.NAME, true);
            //System.Console.WriteLine();
            //await GetBySimilarName("a", "GB", 15, 10, SortOrder.NAME, true);
            //System.Console.WriteLine();

            ////-------------------------------------------------------------

            //await GetBySimilarName("a", "EG", 15, 5);
            //System.Console.WriteLine();
            //await GetBySimilarName("a", "EG", 20, 5);
            //System.Console.WriteLine();
            //await GetBySimilarName("a", "EG", 15, 10);
            //System.Console.WriteLine();

            //await GetBySimilarName("a", "EG", 15, 5, SortOrder.NAME, true);
            //System.Console.WriteLine();
            //await GetBySimilarName("a", "EG", 20, 5, SortOrder.NAME, true);
            //System.Console.WriteLine();
            //await GetBySimilarName("a", "EG", 15, 10, SortOrder.NAME, true);
            //System.Console.WriteLine();

            //await GetBySimilarName("a", "EG", 0, null);
            //System.Console.WriteLine();

            ////-------------------------------------------------------------

            //await GetBySimilarName("a", "GB", null, null);
            //System.Console.WriteLine();

            //await GetBySimilarNameCount("a", "GB");
            //System.Console.WriteLine();

            ////-------------------------------------------------------------

            //await GetBySimilarName("a", "EG", null, null);
            //System.Console.WriteLine();

            //await GetBySimilarNameCount("a", "EG");
            //System.Console.WriteLine();

            ////-------------------------------------------------------------

            //await GetCountryCode("GB");
            //System.Console.WriteLine();

            ////-------------------------------------------------------------

            //await GetCountryCodes();
            //System.Console.WriteLine();

            ////-------------------------------------------------------------

            //await GetFeatureCode("PPL");
            //System.Console.WriteLine();

            ////-------------------------------------------------------------

            //await GetFeatureCodes();
            //System.Console.WriteLine();

            ////-------------------------------------------------------------

            //await GetByFeatureCodeId("ppl", null);
            //System.Console.WriteLine();
            //await GetByFeatureCodeId("ppl", "gb");
            //System.Console.WriteLine();
            //await GetByFeatureCodeId("ppl", "eg");
            //System.Console.WriteLine();
            //await GetByFeatureCodeId("mus", "eg");
            //System.Console.WriteLine();

            ////-------------------------------------------------------------

            //await GetByCountryCodeId("eg");
            //System.Console.WriteLine();
            //await GetByCountryCodeId("GB");
            //System.Console.WriteLine();
        }
    }
}
