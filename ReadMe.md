The project contains a couple of utilities that are useful when working with the [Location Finder RESTful API](https://bitbucket.org/i-s-a/location-finder).

## Location Finder Utility

LocationFinderUtility.exe is a command line tool that you can use to create your own database of locations to use with the Location Finder RESTful API project. The database needs to be populated with a list of country codes; a list of feature codes; a list of locations.

### Country Codes

The country codes require the following format

* Id - This is the ISO 2 character Alpha code for the country (e.g. GB)
* Name - The English name of the country
* Alpha 3 Code - The ISO 3 character Alpha code for the country (e.g. GBR)
* Numeric Code - The ISO 3 digit numeric code for the country (e.g. 826)

The full list of country codes are available here https://en.wikipedia.org/wiki/ISO_3166-1. Alternatively the file CountryCodes.csv available in LocationFinderUtility\LocationFinderClientTest\TestFiles contains all the country codes in a csv file and can be used to populate the database 

### Feature Codes

The feature codes describe the different types of locations that are stored in the database. The feature codes require the following format

* Id - A unique variable sized string representing the feature (e.g. AIRP)
* Name - The name of the feature (e.g. Airport)

The feature codes used in the example https://location-finder-100.herokuapp.com/swagger uses the feature codes found on the [GeoNames website](https://www.geonames.org). A copy of the feature codes from the website can be found in the file featureCodes_en.txt available in LocationFinderUtility\LocationFinderClientTest\TestFiles and can be used to populate the database

### Locations

Each location requires the following format

* Id - A unique signed 64bit value
* Name - A variable sized string
* Country Code - The 2 character ISO country code of the location (e.g. GB, EG)
* Feature Code - A variable sized string representing of location (e.g. AIRP, MUS)
* Latitude - The latitude of the location in degrees
* Longitude - The longitude of the location in degrees

The locations used in the example https://location-finder-100.herokuapp.com/swagger uses the list of locations for the United Kingdom and Northern Ireland downloaded from [GeoNames website](https://www.geonames.org). The website contains millions of locations from around the world and the dataset is free to download. The Location format above is a subset of the format used by [GeoNames](https://www.geonames.org) so you can populate the database with any locations downloaded from there. 


LocationFinderUtility.exe accepts that following command line parameters

**-c** -- Connection string. For the default SQLite database this can simply be the database filename (e.g. locations.db)

**-cc** -- Filename containing the country codes

**-fc** -- Filename containing the feature codes

**-i** -- Filename containing a list of locations


You have to call the utility at least once to initially populate the country codes and feature codes. For example

LocationFinderUtility -c "locations.db" -cc "CountryCodes.csv" -fc "featureCodes_en.txt"

After that you can call the utility as many times as you like to populate new locations or update the locations that already exist. For example

LocationFinderUtility -c "locations.db" -i "UK_locations.txt"

LocationFinderUtility -c "locations.db" -i "Australia_locations.txt"


## Location Finder Client Connection

The project contains a client connection component, implemented in .NET Core 2.2, that can be used to access the Location Finder API directly. For example

	string baseUrl = "https://location-finder-100.herokuapp.com/api/v1/";

	try
	{
		//Centre location
		GeoCoordinates location = new GeoCoordinates { Latitude = 50.82838, Longitude = -0.13947 };

		//2Km radius centred around the location
		double distanceKm = 2;
		
		//All features
		string featureCodeId = null;
		
		//Return the first 20 results
		int offset = 0;
		int limit = 20;

		using (var connection = new LocationFinderClientConnection(baseUrl))
		{
			var response = await connection.GetLocationsWithinBoundary(location, distanceKm, featureCodeId, offset, limit);

			foreach (var r in response)
			{
				Console.WriteLine(
					$"ID: {r.Id},  " +
					$"Name: {r.Name},  " +
					$"Distance Km: {r.DistanceKm}, " +
					$"Miles: {GeoMath.KmToMiles(r.DistanceKm)}\n" +
					$"Country Code: {r.CountryCodeId} " +
					$"Feature Code: {r.FeatureCodeId}");
			}
		}
	}
	catch(Exception ex)
	{
		Console.WriteLine(ex.Message);
	}
