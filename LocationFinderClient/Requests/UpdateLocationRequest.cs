﻿using GeoMathLib;

namespace LocationFinderClient.Requests
{
    public class UpdateLocationRequest
    {
        public string Name { get; set; }
        public string CountryCodeId { get; set; }
        public string FeatureCodeId { get; set; }

        //Location coordinates are always in degrees
        public GeoCoordinates? Coordinates { get; set; }
    }
}
