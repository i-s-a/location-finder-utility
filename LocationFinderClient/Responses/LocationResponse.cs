﻿using GeoMathLib;

namespace LocationFinderClient.Responses
{
    public class LocationResponse
    {
        public long Id { get; set; }
        public string Name { get; set; }

        //Location coordinates are always in degrees
        public GeoCoordinates Coordinates { get; set; }

        //The distance in Km between the location from which we are searching to this
        //location (this->Coordinates)
        public double DistanceKm { get; set; } = 0.0;

        public string CountryCodeId { get; set; }
        public string FeatureCodeId { get; set; }

        //Relative URL that can be used to navigate to this location
        public string HRef { get; set; }
    }
}
