﻿using System;

namespace LocationFinderClient
{
    class ClientConnectionException : Exception
    {
        public ClientConnectionException(string baseUri, string message)
            : base(message)
        {
        }

        public ClientConnectionException(string baseUri, string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public string BaseUri { get; set; }
    }
}
