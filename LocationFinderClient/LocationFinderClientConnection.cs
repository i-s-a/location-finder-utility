﻿using System;
using System.Text;
using System.Net.Http;
using System.Net.Http.Headers;
using Newtonsoft.Json;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Linq;
using LocationFinderClient.Requests;
using LocationFinderClient.Responses;
using GeoMathLib;

namespace LocationFinderClient
{
    public enum SortOrder
    {
        ID,
        NAME,
        LATITUDE,
        LONGITUDE
    };

    public class LocationFinderClientConnection : IDisposable
    {
        public LocationFinderClientConnection(string baseUri)
        {
            Uri uri = new Uri(baseUri);

            _httpClient = new HttpClient
            {
                BaseAddress = uri
            };

            _httpClient.DefaultRequestHeaders.Accept.Clear();
            _httpClient.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        public async Task<HttpResponseMessage> PostAsJson<T>(string uri, T content)
        {
            string json = JsonConvert.SerializeObject(content);

            HttpResponseMessage httpResponseMessage =
                await _httpClient.PostAsync(uri, new StringContent(json, Encoding.UTF8, "application/json"));

            return httpResponseMessage;
        }

        public async Task<HttpResponseMessage> PutAsJson<T>(string uri, T content)
        {
            string json = JsonConvert.SerializeObject(content);

            HttpResponseMessage httpResponseMessage =
            await _httpClient.PutAsync(uri, new StringContent(json, Encoding.UTF8, "application/json"));

            return httpResponseMessage;
        }

        public async Task<Response> DoPost<Request, Response>(string url, Request request)
        {
            using (var response = await PostAsJson<Request>(url, request))
            {
                var readResponse = await response.Content.ReadAsStringAsync();

                if (response.IsSuccessStatusCode)
                {
                    var deserializedResponse = JsonConvert.DeserializeObject<Response>(readResponse);
                    return deserializedResponse;
                }
                else
                {
                    throw new ClientConnectionException(url,
                        string.Format($"{response.StatusCode.ToString()} - {readResponse}"));
                }
            }
        }

        public async Task<Response> DoPut<Request, Response>(string url, Request request)
        {
            using (var response = await PutAsJson<Request>(url, request))
            {
                var putResponse = await response.Content.ReadAsStringAsync();

                if (response.IsSuccessStatusCode)
                {
                    return JsonConvert.DeserializeObject<Response>(putResponse);
                }
                else
                {
                    throw new ClientConnectionException(url,
                        string.Format($"{response.StatusCode.ToString()} - {putResponse}"));
                }
            }
        }

        public async Task<Response> DoDelete<Response>(string url)
        {
            using (var response = await _httpClient.DeleteAsync(url))
            {
                var deleteResponse = await response.Content.ReadAsStringAsync();

                if (response.IsSuccessStatusCode)
                {
                    return JsonConvert.DeserializeObject<Response>(deleteResponse);
                }
                else
                {
                    throw new ClientConnectionException(url,
                        string.Format($"{response.StatusCode.ToString()} - {deleteResponse}"));
                }
            }
        }

        public async Task<Response> DoGet<Response>(string url)
        {
            using (var response = await _httpClient.GetAsync(url))
            {
                var contentResponse = await response.Content.ReadAsStringAsync();

                if (response.IsSuccessStatusCode)
                {
                    return JsonConvert.DeserializeObject<Response>(contentResponse);
                }
                else
                {
                    throw new ClientConnectionException(url,
                        string.Format($"{response.StatusCode.ToString()} - {contentResponse}"));
                }
            }
        }

        public async Task<List<Response>> DoGetMany<Response>(string url)
        {
            using (var response = await _httpClient.GetAsync(url))
            {
                var contentResponse = await response.Content.ReadAsStringAsync();

                if (response.IsSuccessStatusCode)
                {
                    return JsonConvert.DeserializeObject<List<Response>>(contentResponse);
                }
                else
                {
                    throw new ClientConnectionException(url,
                        string.Format($"{response.StatusCode.ToString()} - {contentResponse}"));
                }
            }
        }

        public async Task<int> DoCount(string url)
        {
            int count = 0;

            HttpRequestMessage httpRequestMessage = new HttpRequestMessage
            {
                RequestUri = new Uri(_httpClient.BaseAddress + url),
                Method = HttpMethod.Head
            };

            using (var response = await _httpClient.SendAsync(httpRequestMessage))
            {
                if (response.IsSuccessStatusCode)
                {
                    if (response.Headers.TryGetValues("X-Total-Count", out var values))
                    {
                        var result = values.FirstOrDefault();
                        if (!string.IsNullOrEmpty(result))
                            count = Convert.ToInt32(result);
                    }
                }
                else
                {
                    throw new ClientConnectionException(url,
                        string.Format($"{response.StatusCode.ToString()}"));
                }
            }

            return count;
        }

        private readonly string LOCATIONS_URL = "locations";
        private readonly string FEATURES_URL = "locations/features";
        private readonly string COUNTRIES_URL = "locations/countries";
        private readonly string COUNTRY_CODE_URL = "codes/countries";
        private readonly string FEATURE_CODE_URL = "codes/features";

        public Task<LocationResponse> GetLocationById(long id)
        {
            string uri = string.Format($"{LOCATIONS_URL}/{id}");
            return DoGet<LocationResponse>(uri);
        }

        public Task<List<LocationResponse>> GetLocationByName(string name, string countryCodeId, int? offset,
            int? limit)
        {
            string uri = null;
            if (string.IsNullOrEmpty(countryCodeId))
            {
                uri = string.Format($"{LOCATIONS_URL}/names/{name}");
            }
            else
            {
                uri = string.Format($"{LOCATIONS_URL}/names/{name}/countries/{countryCodeId}");
            }
            
            if (offset.HasValue && limit.HasValue)
            {
                //Offset and limit have been specified
                uri += $"?offset={offset.Value}&limit={limit.Value}";
            }
            else if (limit.HasValue)
            {
                //We can have a limit without an offset. The API will default to offset 0
                uri += $"?limit={limit.Value}";
            }

            return DoGetMany<LocationResponse>(uri);
        }

        public Task<List<LocationResponse>> GetLocationByName(string name, string countryCodeId, int? offset,
            int? limit, SortOrder sortOrder, bool descending)
        {
            string uri = null;
            if (string.IsNullOrEmpty(countryCodeId))
            {
                uri = string.Format($"{LOCATIONS_URL}/names/{name}");
            }
            else
            {
                uri = string.Format($"{LOCATIONS_URL}/names/{name}/countries/{countryCodeId}");
            }

            string sortString = GetSortString(sortOrder, descending);

            if (offset.HasValue && limit.HasValue)
            {
                //Offset and limit have been specified
                uri += $"?offset={offset.Value}&limit={limit.Value}&sort={sortString}";
            }
            else if (limit.HasValue)
            {
                //We can have a limit without an offset. The API will default to offset 0
                uri += $"?limit={limit.Value}&sort={sortString}";
            }
            else
            {
                uri += $"?sort={sortString}";
            }

            return DoGetMany<LocationResponse>(uri);
        }

        public Task<int> GetLocationByNameCount(string name, string countryCodeId)
        {
            string uri = null;
            if (string.IsNullOrEmpty(countryCodeId))
            {
                uri = string.Format($"{LOCATIONS_URL}/names/{name}");
            }
            else
            {
                uri = string.Format($"{LOCATIONS_URL}/names/{name}/countries/{countryCodeId}");
            }

            return DoCount(uri);
        }

        public Task<List<LocationResponse>> GetLocationsBySimilarName(string name, string countryCodeId,
            int? offset, int? limit)
        {
            string uri = null;

            if (string.IsNullOrEmpty(countryCodeId))
            {
                uri = string.Format($"{LOCATIONS_URL}/names/{name}?similar=true");
            }
            else
            {
                uri = string.Format($"{LOCATIONS_URL}/names/{name}/countries/{countryCodeId}?similar=true");
            }

            if (offset.HasValue && limit.HasValue)
            {
                //Offset and limit have been specified
                uri += $"&offset={offset.Value}&limit={limit.Value}";
            }
            else if (limit.HasValue)
            {
                //We can have a limit without an offset. The API will default to offset 0
                uri += $"&limit={limit.Value}";
            }

            return DoGetMany<LocationResponse>(uri);
        }

        public Task<List<LocationResponse>> GetLocationsBySimilarName(string name, string countryCodeId,
            int? offset, int? limit, SortOrder sortOrder, bool descending)
        {
            string uri = null;

            if (string.IsNullOrEmpty(countryCodeId))
            {
                uri = string.Format($"{LOCATIONS_URL}/names/{name}?similar=true");
            }
            else
            {
                uri = string.Format($"{LOCATIONS_URL}/names/{name}/countries/{countryCodeId}?similar=true");
            }

            string sortString = GetSortString(sortOrder, descending);

            if (offset.HasValue && limit.HasValue)
            {
                //Offset and limit have been specified
                uri += $"&offset={offset.Value}&limit={limit.Value}&sort={sortString}";
            }
            else if (limit.HasValue)
            {
                //We can have a limit without an offset. The API will default to offset 0
                uri += $"&limit={limit.Value}&sort={sortString}";
            }
            else
            {
                uri += $"&sort={sortString}";
            }

            return DoGetMany<LocationResponse>(uri);
        }

        public Task<int> GetLocationsBySimilarNameCount(string name, string countryCodeId)
        {
            string uri = null;

            if (string.IsNullOrEmpty(countryCodeId))
            {
                uri = string.Format($"{LOCATIONS_URL}/names/{name}?similar=true");
            }
            else
            {
                uri = string.Format($"{LOCATIONS_URL}/names/{name}/countries/{countryCodeId}?similar=true");
            }

            return DoCount(uri);
        }

        public Task<List<LocationResponse>> GetLocations(int? offset, int? limit)
        {
            string uri = LOCATIONS_URL;

            if (offset.HasValue && limit.HasValue)
            {
                //Offset and limit have been specified
                uri += $"?offset={offset.Value}&limit={limit.Value}";
            }
            else if (limit.HasValue)
            {
                //We can have a limit without an offset. The API will default to offset 0
                uri += $"?limit={limit.Value}";
            }

            return DoGetMany<LocationResponse>(uri);
        }

        public Task<List<LocationResponse>> GetLocations(int? offset, int? limit, SortOrder sortOrder, bool descending)
        {
            string uri = LOCATIONS_URL;

            string sortString = GetSortString(sortOrder, descending);

            if (offset.HasValue && limit.HasValue)
            {
                //Offset and limit have been specified
                uri += $"?offset={offset.Value}&limit={limit.Value}&sort={sortString}";
            }
            else if (limit.HasValue)
            {
                //We can have a limit without an offset. The API will default to offset 0
                uri += $"?limit={limit.Value}&sort={sortString}";
            }
            else
            {
                uri += $"?sort={sortString}";
            }

            return DoGetMany<LocationResponse>(uri);
        }

        public Task<int> GetLocationsCount()
        {
            string uri = LOCATIONS_URL;

            return DoCount(uri);
        }

        public Task<List<LocationResponse>> GetLocationsWithinBoundary(GeoCoordinates location,
            double distance, string featureCodeId, int? offset, int? limit)
        {
            //Get locations within "distance" of "latitude" and "longitude"
            string uri = string.Format($"{LOCATIONS_URL}?lat={location.Latitude}&lon={location.Longitude}&distance={distance}");

            if (!string.IsNullOrEmpty(featureCodeId))
            {
                uri += $"&featureid={featureCodeId}";
            }

            if (offset.HasValue && limit.HasValue)
            {
                //Offset and limit have been specified
                uri += $"&offset={offset.Value}&limit={limit.Value}";
            }
            else if (limit.HasValue)
            {
                //We can have a limit without an offset. The API will default to offset 0
                uri += $"&limit={limit.Value}";
            }

            return DoGetMany<LocationResponse>(uri);
        }

        public Task<List<LocationResponse>> GetLocationsWithinBoundary(GeoCoordinates location,
            double distance, string featureCodeId, int? offset, int? limit, SortOrder sortOrder,
            bool descending)
        {
            //Get locations within "distance" of "latitude" and "longitude"
            string uri = string.Format($"{LOCATIONS_URL}?lat={location.Latitude}&lon={location.Longitude}&distance={distance}");

            if (!string.IsNullOrEmpty(featureCodeId))
            {
                uri += $"&featureid={featureCodeId}";
            }

            string sortString = GetSortString(sortOrder, descending);

            if (offset.HasValue && limit.HasValue)
            {
                //Offset and limit have been specified
                uri += $"&offset={offset.Value}&limit={limit.Value}&sort={sortString}";
            }
            else if (limit.HasValue)
            {
                //We can have a limit without an offset. The API will default to offset 0
                uri += $"&limit={limit.Value}&sort={sortString}";
            }
            else
            {
                uri += $"&sort={sortString}";
            }

            return DoGetMany<LocationResponse>(uri);
        }

        public Task<int> GetLocationsWithinBoundaryCount(GeoCoordinates location, double distance, string featureCodeId)
        {
            //Get locations within "distance" of "latitude" and "longitude"
            string uri = string.Format($"{LOCATIONS_URL}?lat={location.Latitude}&lon={location.Longitude}&distance={distance}");

            if (!string.IsNullOrEmpty(featureCodeId))
            {
                uri += $"&featureid={featureCodeId}";
            }

            return DoCount(uri);
        }

        public Task<LocationResponse> CreateLocation(NewLocationRequest request)
        {
            return DoPost<NewLocationRequest, LocationResponse>(LOCATIONS_URL, request);
        }

        public Task<LocationResponse> CreateOrUpdate(long id, UpdateLocationRequest request)
        {
            string uri = string.Format($"{LOCATIONS_URL}/{id}");
            return DoPut<UpdateLocationRequest, LocationResponse>(uri, request);
        }

        public Task<bool> DeleteLocation(long id)
        {
            string uri = string.Format($"{LOCATIONS_URL}/{id}");
            return DoDelete<bool>(uri);
        }

        public Task<List<LocationResponse>> GetByFeatureCodeId(string featureCodeId, string countryCodeId,
            int? offset, int? limit, SortOrder sortOrder, bool descending)
        {
            string uri = string.Format($"{FEATURES_URL}/{featureCodeId}");

            if (!string.IsNullOrEmpty(countryCodeId))
            {
                uri += $"/countries/{countryCodeId}";
            }

            string sortString = GetSortString(sortOrder, descending);

            if (offset.HasValue && limit.HasValue)
            {
                //Offset and limit have been specified
                uri += $"?offset={offset.Value}&limit={limit.Value}&sort={sortString}";
            }
            else if (limit.HasValue)
            {
                //We can have a limit without an offset. The API will default to offset 0
                uri += $"?limit={limit.Value}&sort={sortString}";
            }
            else
            {
                uri += $"?sort={sortString}";
            }

            return DoGetMany<LocationResponse>(uri);
        }

        public Task<int> GetByFeatureCodeIdCount(string featureCodeId, string countryCodeId)
        {
            string uri = string.Format($"{FEATURES_URL}/{featureCodeId}");

            if (!string.IsNullOrEmpty(countryCodeId))
            {
                uri += $"/countries/{countryCodeId}";
            }

            return DoCount(uri);
        }

        public Task<List<LocationResponse>> GetByCountryCodeId(string countryCodeId,
            int? offset, int? limit, SortOrder sortOrder, bool descending)
        {
            string uri = string.Format($"{COUNTRIES_URL}/{countryCodeId}");

            string sortString = GetSortString(sortOrder, descending);

            if (offset.HasValue && limit.HasValue)
            {
                //Offset and limit have been specified
                uri += $"?offset={offset.Value}&limit={limit.Value}&sort={sortString}";
            }
            else if (limit.HasValue)
            {
                //We can have a limit without an offset. The API will default to offset 0
                uri += $"?limit={limit.Value}&sort={sortString}";
            }
            else
            {
                uri += $"?sort={sortString}";
            }

            return DoGetMany<LocationResponse>(uri);
        }

        public Task<int> GetByCountryCodeIdCount(string countryCodeId)
        {
            string uri = string.Format($"{COUNTRIES_URL}/{countryCodeId}");
            return DoCount(uri);
        }



        public Task<CountryCodeResponse> GetCountryCode(string id)
        {
            string uri = string.Format($"{COUNTRY_CODE_URL}/{id}");
            return DoGet<CountryCodeResponse>(uri);
        }

        public Task<List<CountryCodeResponse>> GetCountryCodes()
        {
            string uri = string.Format($"{COUNTRY_CODE_URL}");
            return DoGetMany<CountryCodeResponse>(uri);
        }

        public Task<FeatureCodeResponse> GetFeatureCode(string id)
        {
            string uri = string.Format($"{FEATURE_CODE_URL}/{id}");
            return DoGet<FeatureCodeResponse>(uri);
        }

        public Task<List<FeatureCodeResponse>> GetFeatureCodes()
        {
            string uri = string.Format($"{FEATURE_CODE_URL}");
            return DoGetMany<FeatureCodeResponse>(uri);
        }

        private string GetSortString(SortOrder sortOrder, bool descending)
        {
            switch (sortOrder)
            {
                case SortOrder.ID:
                    return (descending ? "-id" : "id");
                case SortOrder.NAME:
                    return (descending ? "-name" : "name");
                case SortOrder.LATITUDE:
                    return (descending ? "-lat" : "lat");
                case SortOrder.LONGITUDE:
                    return (descending ? "-lon" : "lon");
                default:
                    throw new ArgumentException("Invalid sort order");
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!mDisposed)
            {
                if (disposing)
                {
                    _httpClient.Dispose();
                }

                mDisposed = true;
            }
        }

        ~LocationFinderClientConnection()
        {
            Dispose(false);
        }

        private HttpClient _httpClient = null;
        private bool mDisposed = false;

    }
}
